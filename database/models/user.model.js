const { Schema, model } = require('mongoose');

const userSchema = new Schema({
  id: {
    type: Number
  },
  name: {
    type: String,
    trim: true
  },
  desctiprion: {
    type: String,
    trim: true
  },
  mbti: {
    type: String,
    trim: true
  },
  enneagram: {
    type: String,
    trim: true
  },
  variant: {
    type: String,
    trim: true
  },
  tritype: {
    type: Number
  },
  socionics: {
    type: String,
    trim: true
  },
  sloan: {
    type: String,
    trim: true
  },
  psyche: {
    type: String,
    trim: true
  },
  image: {
    type: String,
    trim: true
  }
});

module.exports = model('User', userSchema);