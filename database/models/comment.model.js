const { Schema, model } = require('mongoose');

const voteSchema = new Schema({ name: 'string' });

const likeSchema = new Schema({ accountId: 'string' })

const commentSchema = new Schema({
  id: {
    type: Number
  },
  profileId: {
    type: String
  },
  accountId: {
    type: String
  },
  accountName: {
    type: String,
    trim: true
  },
  title: {
    type: String,
    trim: true
  },
  body: {
    type: String,
    trim: true
  },
  likes: [likeSchema],
  vote: [voteSchema],
  createdAt: {
    type: String,
    trim: true
  }
});

module.exports = model('Comment', commentSchema);