const User = require('../database/models/user.model');
const { fakeUserData } = require('../database/data/user');
const request = require('supertest')
const server = require('../app')
const {
  validateNotEmpty,
  validateStringEquality,
  validateMongoDuplicationError,
} = require('../utils/validator.utils');
const {
  dbOpen,
  dbDrop,
} = require('../utils/dbHandler.utils');

beforeAll(async () => dbOpen());
afterAll(async () => dbDrop());

describe('User Model Test Suite', () => {
  test('should validate saving a new student user successfully', async () => {
    const validUser = new User({
      id: fakeUserData.id,
      name: fakeUserData.name,
      desctiprion: fakeUserData.description,
      mbti: fakeUserData.mbti,
      enneagram: fakeUserData.enneagram,
      variant: fakeUserData.variant,
      tritype: fakeUserData.tritype,
      socionics: fakeUserData.socionics,
      sloan: fakeUserData.sloan,
      psyche: fakeUserData.psyche,
      image: fakeUserData.image
    });
    const savedUser = await validUser.save();

    validateNotEmpty(savedUser);
    validateStringEquality(savedUser.name, fakeUserData.name);
    validateStringEquality(
      savedUser.desctiprion,
      fakeUserData.description
    );
    validateStringEquality(
      savedUser.enneagram,
      fakeUserData.enneagram
    );
    validateStringEquality(
      savedUser.mbti,
      fakeUserData.mbti
    );
    validateStringEquality(
      savedUser.socionics,
      fakeUserData.socionics
    );
  });

  test('should validate MongoError duplicate error with code 11000', async () => {
    const validUser = new User({
      id: fakeUserData.id,
      name: fakeUserData.name,
      desctiprion: fakeUserData.description,
      mbti: fakeUserData.mbti,
      enneagram: fakeUserData.enneagram,
      variant: fakeUserData.variant,
      tritype: fakeUserData.tritype,
      socionics: fakeUserData.socionics,
      sloan: fakeUserData.sloan,
      psyche: fakeUserData.psyche,
      image: fakeUserData.image
    });

    try {
      await validUser.save();
    } catch (error) {
      const { name, code } = error;
      validateMongoDuplicationError(name, code);
    }
  });
});

describe('Post Endpoints Test Suite', () => {
  test('should create a new user', done => {
    request(server)
      .post('/')
      .send({
        id: fakeUserData.id,
        name: fakeUserData.name,
        desctiprion: fakeUserData.description,
        mbti: fakeUserData.mbti,
        enneagram: fakeUserData.enneagram,
        variant: fakeUserData.variant,
        tritype: fakeUserData.tritype,
        socionics: fakeUserData.socionics,
        sloan: fakeUserData.sloan,
        psyche: fakeUserData.psyche,
        image: fakeUserData.image
      }).then(res => {
        expect(res.statusCode).toEqual(200)
        expect(res.ok).toEqual(true)
        done();
      })
  })
  test('should create a new comment', done => {
    request(server)
      .post('/1/comment')
      .send({
        id: 2,
        profileId: 1,
        accountId: 2,
        accountName: 'New User',
        title: 'Test Comment',
        body: 'Body Comment',
        createdAt: new Date()
      }).then(res => {
        expect(res.statusCode).toEqual(200)
        expect(res.ok).toEqual(true)
        done();
      })
  })
  test('should create a new vote', done => {
    request(server)
      .post('/1/vote')
      .send({
        id: 2,
        vote: '2qwe'
      }).then(res => {
        expect(res.statusCode).toEqual(200)
        expect(res.ok).toEqual(true)
        done();
      })
  })
  test('should create a new like', done => {
    request(server)
      .post('/1/like')
      .send({
        id: 2,
        accountId: 1
      }).then(res => {
        expect(res.statusCode).toEqual(200)
        expect(res.ok).toEqual(true)
        done();
      })
  })
})

describe('Get Endpoints Test Suite', () => {
  test('should get user detail', done => {
    request(server)
      .get('/1')
      .then(res => {
        expect(res.statusCode).toEqual(200)
        expect(res.ok).toEqual(true)
        done();
      })
  })
})