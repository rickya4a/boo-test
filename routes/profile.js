'use strict';

const express = require('express');
const router = express.Router();
const User = require('../database/models/user.model');
const Comment = require('../database/models/comment.model')
const {
  dbOpen,
  dbClose,
} = require('../utils/dbHandler.utils');

const profiles = [
  {
    "id": 1,
    "name": "A Martinez",
    "description": "Adolph Larrue Martinez III.",
    "mbti": "ISFJ",
    "enneagram": "9w3",
    "variant": "sp/so",
    "tritype": 725,
    "socionics": "SEE",
    "sloan": "RCOEN",
    "psyche": "FEVL",
    "image": "https://soulverse.boo.world/images/1.png",
  }
];

module.exports = function () {

  router.get('/', function (req, res, next) {
    res.render('profile_template', {
      profile: profiles[0],
    });
  });

  router.get('/:userId', async (req, res, next) => {
    const { params, query } = req
    
    const user = await User.find({ id: params.userId });

    let comments;

    if (query.sort) {
      comments = await Comment
      .find({ profileId: params.userId })
      .sort({
        ...(query.sort === 'recent' ?
          { createdAt: -1 } :
          { likes: -1 }
        )
      })
    } else if (query.filter) {
      comments = await Comment.find({ profileId: params.userId, [query.filter]: -1 })
    } else if (query.filter && query.sort) {
      comments = await Comment
      .find({ profileId: params.userId, [query.filter]: -1 })
      .sort({
        ...(query.sort === 'recent' ?
          { createdAt: -1 } :
          { likes: -1 }
        )
      })
    } else {
      comments = await Comment.find({ profileId: params.userId })
    }

    res.json({ user, comments })
  })

  router.post('/:userId/comment', async (req, res, next) => {

    const { body, params } = req;

    const validComment = new Comment({
      id: body.id,
      profileId: params.userId,
      accountId: body.accountId,
      accountName: body.accountName,
      title: body.title,
      body: body.bodyComment,
      createdAt: new Date()
    });
    const savedCommentUser = await validComment.save();

    res.json(savedCommentUser)
  })

  router.post('/:commentId/vote', async (req, res, next) => {
    const result = await Comment.findOneAndUpdate(
      { id: req.params.commentId },
      {
        $push: {
          vote: {
            name: req.body.vote
          }
        }
      },
      {
        new: true
      }
    )

    res.json(result)
  })

  router.post('/:commentId/like', async (req, res, next) => {
    const result = await Comment.findOneAndUpdate(
      { id: req.params.commentId },
      {
        $push: {
          likes: {
            accountId: req.body.accountId
          }
        }
      },
      {
        new: true
      }
    )

    res.json(result)
  })

  router.post('/', async (req, res) => {

    const { body } = req;

    const validUser = new User({
      id: body.id,
      name: body.name,
      desctiprion: body.description,
      mbti: body.mbti,
      enneagram: body.enneagram,
      variant: body.variant,
      tritype: body.tritype,
      socionics: body.socionics,
      sloan: body.sloan,
      psyche: body.psyche,
      image: body.image
    });
    const savedUser = await validUser.save();

    res.json(savedUser)
  })

  return router;
}

