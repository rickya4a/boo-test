'use strict';

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');

if (process.env.NODE_ENV !== 'test') {
  (async () => {
    const mongo = await MongoMemoryServer.create({
      instance: {
        dbName: "boo"
      }
    });
    const uri = mongo.getUri();

    await mongoose.connect(uri);
  
    app.set('view engine', 'ejs');
    
    app.use(bodyParser.json());
    
    app.use(bodyParser.urlencoded({
      extended: true
    }));
    
    // routes
    app.use('/', require('./routes/profile')());
  })();
} else {
  app.set('view engine', 'ejs');
  
  app.use(bodyParser.json());
  
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  
  // routes
  app.use('/', require('./routes/profile')());
}

module.exports = app;