const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');

const mongoServer = new MongoMemoryServer();

exports.dbOpen = async () => {
  const mongo = await MongoMemoryServer.create({
    instance: {
      dbName: "boo"
    }
  });
  const uri = mongo.getUri();

  await mongoose.connect(uri);
};

exports.dbClose = async () => {
  await mongoose.connection.close();
  await mongoServer.stop();
};

exports.dbDrop = async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
  await mongoServer.stop();
};